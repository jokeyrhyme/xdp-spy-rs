# xdp-hook-rs [![Status](https://img.shields.io/badge/status-actively--developed-brightgreen)](https://gitlab.com/jokeyrhyme/xdp-hook-rs) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/xdp-hook-rs?branch=main)](https://gitlab.com/jokeyrhyme/xdp-hook-rs/-/pipelines?ref=main) [![Crates.io](https://img.shields.io/crates/v/xdp-hook)](https://crates.io/crates/xdp-hook) [![docs.rs](https://img.shields.io/docsrs/xdp-hook)](https://docs.rs/xdp-hook)

trigger user-configured actions when other apps use xdg-desktop-portal ( https://github.com/flatpak/xdg-desktop-portal )

## what?

- [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal)s are a mechanism built on top of [dbus](https://www.freedesktop.org/wiki/Software/dbus/) and [pipewire](https://pipewire.org/),
  used by Linux apps when direct access to sensitive system capabilities is unavailable
  (or when explicit user consent is desirable)

- we can use [`dbus-monitor`](https://dbus.freedesktop.org/doc/dbus-monitor.1.html) to observe the negotiation and usage of such portals,
  but it doesn't make it convenient to create a bird's-eye view of active sessions,
  and might not be the easiest way to trigger portal-related automation, etc

- `xdp-hook`'s purpose is not to offer [privacy indicators like those on Android](https://source.android.com/devices/tech/config/privacy-indicators),
  but rather to make it easier for others to offer such features,
  by reacting to portal usage and performing user-configured tasks e.g. executing scripts

- this project depends upon Linux and offers functionality for Linux systems
  (no support for other environments)

## installation

### from https://crates.io/ via `cargo`

- ensure you have a working Rust toolchain: https://rustup.rs/

- run `cargo install xdp-hook`

## configuration

- create ~/.config/xdp-hook/config.toml (see [example configuration](./config.example.toml))

### hook environment

when `xdp-hook` runs a hook that you have configured,
it attempts to set certain environment variables to expose more information:

- `XDP_HOOK_APP_ID`: the name of the (flatpak app-like) process (if any) that initiated the portal session
- `XDP_HOOK_PROCESS_COMMAND`: the name of the process that initiated the portal session
- `XDP_HOOK_PROCESS_ID`: the process ID of the process that initiated the portal session

note that these are "best-effort" and might be empty strings when information is unavailable

## features/roadmap

- [x] connect to user's dbus session
- [x] identify and log out general messages relating to portal requests
- [x] track open/close for prolonged portal sessions (e.g. Location, ScreenCast, RemoteDesktop)
- [ ] track open/close for prolonged portal sessions (e.g. Inhibit)
- [x] read configuration file ~/.config/xdp-hook/config.toml
- [x] perform user-configured tasks upon portal request/session event e.g. executing scripts
- [x] user-scope systemd unit file for convenient startup
- [x] properly differentiate between concurrent clients for portals (e.g. both Chrome and Firefox using [jitsi](https://meet.jit.si/)): https://gitlab.com/jokeyrhyme/xdp-hook-rs/-/issues/3
- [x] identify app/process that is initiating the portal request: https://gitlab.com/jokeyrhyme/xdp-hook-rs/-/issues/3
- [ ] identify the portal implementation that handles the portal request (impossible?)

## see also

- https://github.com/flatpak/xdg-desktop-portal

- https://www.freedesktop.org/wiki/Software/dbus/

- https://pipewire.org/

- https://dbus.freedesktop.org/doc/dbus-monitor.1.html

- https://source.android.com/devices/tech/config/privacy-indicators

- https://meet.jit.si/

- https://crates.io/crates/zbus and https://crates.io/crates/zvariant
