use zbus::{names::InterfaceName, Message, MessageType};

pub(crate) enum Error {
    MissingInterface,
    UnknownInterface,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum Interface {
    Location,
    ScreenCast,
    RemoteDesktop,
}
impl TryFrom<Option<InterfaceName<'_>>> for Interface {
    type Error = Error;

    fn try_from(value: Option<InterfaceName>) -> Result<Self, Self::Error> {
        match value {
            Some(dbus_interface) => match dbus_interface.as_str() {
                INTERFACE_LOCATION => Ok(Interface::Location),
                INTERFACE_SCREEN_CAST => Ok(Interface::ScreenCast),
                INTERFACE_REMOTE_DESKTOP => Ok(Interface::RemoteDesktop),
                _ => Err(Error::UnknownInterface),
            },
            None => Err(Error::MissingInterface),
        }
    }
}

pub(crate) fn is_create_session(msg: &Message) -> bool {
    if let (Some(i), Some(m)) = (msg.interface(), msg.member()) {
        let interface = i.as_str();
        let method = m.as_str();
        return msg.message_type() == MessageType::MethodCall
            && method == MEMBER_CREATE_SESSION
            && (interface == INTERFACE_LOCATION
                || interface == INTERFACE_REMOTE_DESKTOP
                || interface == INTERFACE_SCREEN_CAST);
    }
    false
}

pub(crate) fn is_portal_interface(msg: &Message) -> bool {
    if let Some(interface) = msg.interface() {
        return interface.as_str().starts_with(INTERFACE_PREFIX);
    }
    false
}

pub(crate) fn is_request_response(msg: &Message) -> bool {
    if let (Some(i), Some(m)) = (msg.interface(), msg.member()) {
        let interface = i.as_str();
        let method = m.as_str();
        return msg.message_type() == MessageType::Signal
            && method == MEMBER_RESPONSE
            && interface == INTERFACE_REQUEST;
    }
    false
}

pub(crate) fn is_session_close(msg: &Message) -> bool {
    if let (Some(i), Some(m)) = (msg.interface(), msg.member()) {
        let interface = i.as_str();
        let method = m.as_str();
        return msg.message_type() == MessageType::MethodCall
            && method == MEMBER_CLOSE
            && interface == INTERFACE_SESSION;
    }
    false
}

const INTERFACE_LOCATION: &str = "org.freedesktop.portal.Location";
const INTERFACE_PREFIX: &str = "org.freedesktop.portal.";
const INTERFACE_REMOTE_DESKTOP: &str = "org.freedesktop.portal.RemoteDesktop";
const INTERFACE_REQUEST: &str = "org.freedesktop.portal.Request";
const INTERFACE_SCREEN_CAST: &str = "org.freedesktop.portal.ScreenCast";
const INTERFACE_SESSION: &str = "org.freedesktop.portal.Session";

const MEMBER_CLOSE: &str = "Close";
const MEMBER_CREATE_SESSION: &str = "CreateSession";
const MEMBER_RESPONSE: &str = "Response";

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_create_session_returns_false() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.portal.ScreenCast"),
            "SelectSources",
            &(),
        )
        .expect("unable to create test message");
        let got = is_create_session(&input);
        assert!(!got);
    }

    #[test]
    fn is_create_session_returns_true() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.portal.ScreenCast"),
            "CreateSession",
            &(),
        )
        .expect("unable to create test message");
        let got = is_create_session(&input);
        assert!(got);
    }

    #[test]
    fn is_portal_interface_returns_false() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.impl.portal.ScreenCast"),
            "Foo",
            &(),
        )
        .expect("unable to create test message");
        let got = is_portal_interface(&input);
        assert!(!got);
    }

    #[test]
    fn is_portal_interface_returns_true() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.portal.Session"),
            "Foo",
            &(),
        )
        .expect("unable to create test message");
        let got = is_portal_interface(&input);
        assert!(got);
    }

    #[test]
    fn is_request_response_returns_false() {
        let input = Message::signal(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            "org.freedesktop.portal.Request",
            "Request",
            &(),
        )
        .expect("unable to create test message");
        let got = is_request_response(&input);
        assert!(!got);
    }

    #[test]
    fn is_request_response_returns_true() {
        let input = Message::signal(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            "org.freedesktop.portal.Request",
            "Response",
            &(),
        )
        .expect("unable to create test message");
        let got = is_request_response(&input);
        assert!(got);
    }

    #[test]
    fn is_session_close_returns_false() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.portal.Request"),
            "Close",
            &(),
        )
        .expect("unable to create test message");
        let got = is_session_close(&input);
        assert!(!got);
    }

    #[test]
    fn is_session_close_returns_true() {
        let input = Message::method(
            Some(":9.999"),
            Some(":9.999"),
            "/foo",
            Some("org.freedesktop.portal.Session"),
            "Close",
            &(),
        )
        .expect("unable to create test message");
        let got = is_session_close(&input);
        assert!(got);
    }
}
