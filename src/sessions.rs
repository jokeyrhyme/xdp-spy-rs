use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

use crate::{config::Config, hook, portal::Interface};

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct Session {
    pub(crate) interface: Interface,
    pub(crate) process_id: u32,
    pub(crate) state: State,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum State {
    Requested, // detected a request to CreateSession
    Created,   // detected that the response to CreateSession is successful
}

pub(crate) async fn session_closed(
    session_path: String,
    cfg: &Config,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) {
    let outcome = update_sessions_with_close(&session_path, sessions);
    trigger_hooks(outcome, cfg).await;
}

pub(crate) async fn session_created(
    session_path: String,
    cfg: &Config,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) {
    let outcome = update_sessions_with_create(&session_path, sessions);
    trigger_hooks(outcome, cfg).await;
}

pub(crate) async fn session_requested(
    session_path: String,
    interface: Interface,
    process_id: u32,
    cfg: &Config,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) {
    let outcome = update_sessions_with_request(&session_path, &interface, process_id, sessions);
    trigger_hooks(outcome, cfg).await;
}

async fn trigger_hooks(outcome: Outcome, cfg: &Config) {
    match outcome {
        Outcome::AllSessionsClosed(s) => {
            hook::session_closed(&s, cfg).await;
            hook::all_sessions_closed(&s.interface, cfg).await;
        }
        Outcome::NoChange => {
            // noop:
            // nothing interesting happened
        }
        Outcome::SessionClosed(s) => hook::session_closed(&s, cfg).await,
        Outcome::SessionCreated(s) => hook::session_created(&s, cfg).await,
        Outcome::SessionRequested(_) => {
            // noop:
            // for State::Requested, no permissions have yet been approved,
            // so there's nothing of interest to report here
        }
    }
}

#[derive(Debug, PartialEq)]
enum Outcome {
    /// note: AllSessionsClosed implies singular SessionClosed
    AllSessionsClosed(Session),
    NoChange,
    SessionClosed(Session),
    SessionCreated(Session),
    SessionRequested(Session),
}

fn update_sessions_with_close(
    path: &str,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) -> Outcome {
    let mut sessions = sessions.lock().expect("unable to lock session state");
    match sessions.remove(path) {
        Some(s) => {
            let remaining = sessions
                .values()
                .filter(|sm| sm.interface == s.interface)
                .count();
            if remaining == 0 {
                Outcome::AllSessionsClosed(s)
            } else {
                Outcome::SessionClosed(s)
            }
        }
        None => Outcome::NoChange,
    }
}

fn update_sessions_with_create(
    path: &str,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) -> Outcome {
    let mut outcome = Outcome::NoChange;

    let mut sessions = sessions.lock().expect("unable to lock session state");
    sessions.entry(String::from(path)).and_modify(|s| {
        if s.state != State::Created {
            s.state = State::Created;
            outcome = Outcome::SessionCreated(s.clone());
        }
    });
    outcome
}

fn update_sessions_with_request(
    path: &str,
    interface: &Interface,
    process_id: u32,
    sessions: Arc<Mutex<HashMap<String, Session>>>,
) -> Outcome {
    let session = Session {
        interface: interface.clone(),
        process_id,
        state: State::Requested,
    };

    let mut sessions = sessions.lock().expect("unable to lock session state");
    sessions.insert(String::from(path), session.clone());
    Outcome::SessionRequested(session)
    // TODO: return Outcome::None when appropriate
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn update_sessions_with_close_with_existing_sessions_on_same_interface() {
        let session_path = String::from("path/123");
        let mut sessions: HashMap<String, Session> = HashMap::new();
        let session = Session {
            interface: Interface::Location,
            process_id: 123,
            state: State::Requested,
        };
        sessions.insert(session_path.clone(), session.clone());
        sessions.insert(
            String::from("path/456"),
            Session {
                interface: Interface::Location,
                process_id: 456,
                state: State::Requested,
            },
        );

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_close(&session_path, sessions.clone());

        let sessions = sessions.lock().expect("unable to lock session state");
        assert_eq!(Outcome::SessionClosed(session), got);
        assert_eq!(1, sessions.len());
        assert!(sessions.get("path/456").is_some());
    }

    #[test]
    fn update_sessions_with_close_with_existing_sessions_on_different_interfaces() {
        let session_path = String::from("path/123");
        let mut sessions: HashMap<String, Session> = HashMap::new();
        let session = Session {
            interface: Interface::Location,
            process_id: 123,
            state: State::Requested,
        };
        sessions.insert(session_path.clone(), session.clone());
        sessions.insert(
            String::from("path/456"),
            Session {
                interface: Interface::ScreenCast,
                process_id: 456,
                state: State::Requested,
            },
        );

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_close(&session_path, sessions.clone());

        let sessions = sessions.lock().expect("unable to lock session state");
        assert_eq!(Outcome::AllSessionsClosed(session), got);
        assert_eq!(1, sessions.len());
        assert!(sessions.get("path/456").is_some());
    }

    #[test]
    fn update_sessions_with_close_with_missing_session() {
        let sessions: HashMap<String, Session> = HashMap::new();

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_close("path/456", sessions.clone());

        let sessions = sessions.lock().expect("unable to lock session state");
        assert_eq!(Outcome::NoChange, got);
        assert_eq!(0, sessions.len());
    }

    #[test]
    fn update_sessions_with_create_with_existing_session() {
        let session_path = String::from("path/123");
        let mut sessions: HashMap<String, Session> = HashMap::new();
        sessions.insert(
            session_path.clone(),
            Session {
                interface: Interface::Location,
                process_id: 123,
                state: State::Requested,
            },
        );

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_create(&session_path, sessions.clone());

        let sessions = sessions.lock().expect("unable to lock session state");
        let want_session = Session {
            interface: Interface::Location,
            process_id: 123,
            state: State::Created,
        };
        assert_eq!(Outcome::SessionCreated(want_session.clone()), got);
        assert_eq!(1, sessions.len());
        assert_eq!(Some(&want_session), sessions.get(&session_path),);
    }

    #[test]
    fn update_sessions_with_create_with_missing_session() {
        let sessions: HashMap<String, Session> = HashMap::new();

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_create("path/456", sessions.clone());

        let sessions = sessions.lock().expect("unable to lock session state");
        assert_eq!(Outcome::NoChange, got);
        assert_eq!(0, sessions.len());
    }

    #[test]
    fn update_sessions_with_request_with_new_session() {
        let session_path = String::from("path/123");
        let sessions: HashMap<String, Session> = HashMap::new();

        let sessions = Arc::new(Mutex::new(sessions));
        let got = update_sessions_with_request(
            &session_path,
            &Interface::ScreenCast,
            123,
            sessions.clone(),
        );

        let sessions = sessions.lock().expect("unable to lock session state");
        let want_session = Session {
            interface: Interface::ScreenCast,
            process_id: 123,
            state: State::Requested,
        };
        assert_eq!(Outcome::SessionRequested(want_session.clone()), got);
        assert_eq!(1, sessions.len());
        assert_eq!(Some(&want_session), sessions.get(&session_path),);
    }
}
