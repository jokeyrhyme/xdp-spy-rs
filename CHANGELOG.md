# Changelog

We document all notable changes to this project in this file.

We use the format based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2022-04-24

### Added

- expand `"~/"` at the start of hook commands to the user's home directory
- example systemd user unit file: [xdp-hook.service](./xdp-hook.service)
- set `XDP_HOOK_APP_ID` environment variable when running hook
- set `XDP_HOOK_PROCESS_COMMAND` environment variable when running hook
- set `XDP_HOOK_PROCESS_ID` environment variable when running hook

## [0.1.2] - 2022-04-10

### Added

- new "all_sessions_closed" hook for Location, ScreenCast, and RemoteDesktop interfaces

## [0.1.1] - 2022-04-09

### Fixed

- some clippy warnings

## [0.1.0] - 2022-04-09
